const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = (env, argv) => {
    return [{
        bail: !argv.watch,
        devtool: 'source-map',
        entry: {
            index: './src/index.js'
        },
        output: {
            filename: 'main.js',
            path: path.join(__dirname, 'dist', 'js'),
            libraryTarget: 'umd',
            library: 'main'
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: [
                        {
                            loader: 'babel-loader',
                            options: {
                                presets: ['@babel/preset-env']
                            }
                        },
                    ]
                },
                {
                    // Process CSS files.
                    test: /\.css$/i,
                    use: ['style-loader', 'css-loader'],
                },
            ]
        },
        plugins: [
            new CleanWebpackPlugin({
                watch: true,
                dangerouslyAllowCleanPatternsOutsideProject: true,
                dry: false
            }),
            new CopyPlugin({
                patterns: [{
                    from: path.resolve(__dirname, 'src/assets').replace(/\\/g, "/"),
                    to: path.resolve(__dirname, 'dist/assets').replace(/\\/g, "/"),
                    force: true,
                    noErrorOnMissing: true,
                },
                {
                    from: path.resolve(__dirname, 'src/index.html').replace(/\\/g, "/"),
                    to: path.resolve(__dirname, 'dist').replace(/\\/g, "/"),
                    force: true,
                    noErrorOnMissing: true,
                }
            ],
            }),
        ],
    }];
}; // module exports
