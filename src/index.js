import * as Mobilizing from '@mobilizing/library';
import { Script } from "./script.js";
import "./index.css";

// We need to run the script in a Mobilizing Context
// Build a simple function to instantiate the Context,
// instantiate the user script,
// add it as a component to the Context,
// and build a runner with it.
async function run() {
    const context = new Mobilizing.Context();
    const script = new Script();
    context.addComponent(script);

     //check the script for the need of user interaction
     if (script.needsUserInteraction) {
        context.userInteractionDone.setup();
        await context.userInteractionDone.promise;
    }
    const runner = new Mobilizing.Runner( {context} );
}

// Then run it
run();
