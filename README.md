# Mobilizing.js software environment

## Base example for Moblizing.js Library

This is a base example that has been made as a starting point to use the Moblizing.js Library.

It will give you a fast and easy way to make your own project using the library, as the development if preconfigured for you.
After some initial installation steps (see below), you will be able to write your Mobilizing.js script in the file ```src/script.js```. Thanks to Webpack, you will be able to produce a ditribution bundle f your script with all its assets. It will be located in ```dist```.
It means that if you want to put your project online, we will need to upload the content of the ```dist``` folder to your server.

## Mobilizing.js
Mobilizing.js is a software authoring environment for artists and designers intended to encourage the creation of interactive works on various forms of device-screens. The great versatility of the increasingly well-established JavaScript makes it possible for Mobizling.js to expand its field of action to many software environments, including browsers, Node.js servers, or even the contexts specific to certain machines (mobile devices—tablets, smartphones—iOS, Android, Windows, etc.). The foundations of Mobilizing.js take shape as a JavaScript library that specifies a programming interface developed for interactive artistic creation in hardware and software environments that are now unified by the JavaScript language.

It is developped at EnsadLab, the research laboratory of Ecole nationale supérieure des Arts Décoratifs de Paris, Reflective Interaction research Program, by Dominique Cunin, Jonathan Tanant, Oussama Mubarak.

[Mobilizing Library sources](https://gitlab.com/mobilizing-js/library)

[Mobilizing website](https://mobilizing-js.net/)

## Requierment

Node.js version 10 and upper is needed. Go to download it here : [Node.js download](https://nodejs.org/en/download/)
Some basic understanding of the Terminal is requiered to use the provided commands.
JavaScript (ES6) skills are needed, as Mobilizing.js is entirely written with this language.

## Usage

• Clone this repository to have it on your local disk :

> cd <your/folder/path>

> git clone https://gitlab.com/mobilizing-js/mobilizing-library-base_example.git

• Install all the needed dependecies :

> cd library-base_example

> npm install

**NB :** You can change the name of the folder to match your project name, it won't block future updates through ```git pull```.

**Make a copy of  the file ```src/script.js.sample``` as ```src/script.js```.** This is important that you make the copy yourself as this file will be untracked by git, then you'll be able to use ```git pull``` to update your project from the repository if needed.

Then write your script using Mobilizing.js in the file ```src/script.js``` using your favorite code editor.


## Build and test

To build your project :

> npm run build

You can automatically build a distribution bundle in the ```dist``` folder at every change of your source code with :

> npm run watch

To test things locally, use a local server with :

> npm run start

and follows the URL (such as http://127.0.0.1:8080) it will give you back in the Terminal.

**NB :** Beware of your **navigator cache**, that may block reloading of newly compiled files! You should **disable cache** in your browser when you test your project!

### Assets management

You can add any file or folder you need to populate your project : audio files, font, image for texturing, JSON for home made datavizualisation, etc.
The ```src/assets``` is made for that : add your files in there and the whole ```src/assets``` will be copied to ```dist/assets``` at bundle creation.

## Testing with local server on mobile phone : SSL troubles

In order to test your project on a mobile phone, on your local network, with a local server, you will face some security issues avoiding the use of microphone, camera and any other embeded sensors. That a pain.
To make your tests anyway, you can use a redirection to https. To do so use :

> npm run start-ssl

**Note that this is a cheap hack** only for local testing purpose. You will see an alert diplayed by your browser saying that the page isn't safe to visit. If you visit the page anyway, you should be able to test your project without problem.

## Update to the last version of Mobilizing.js library

Simply use :

> npm update @mobilizing/library

It will update all the npm packages used in this project.

## Update your local repository against the distant one

To get the last version of this repository (which means, not of the Mobilizing library, that can be updated through ```npm update```), use this command from your repository :

> git pull

**NB :** If you change any file other than ```script.js``` that is tracked by git in your local repository, you won't be able to ```pull``` the modifications from the distant repository. You would need to ```stash``` your modifications, then ```pull``` then restore your stashed files.

Happy Mobilizing Coding!